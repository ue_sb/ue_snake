#include "SnakeGameMode.h"
#include "FixedCameraPawn.h"
#include "SnakeActor.h"

ASnakeGameMode::ASnakeGameMode()
{
	DefaultPawnClass = AFixedCameraPawn::StaticClass();
	SnakeClass = ASnakeActor::StaticClass();	
}

void ASnakeGameMode::BeginPlay()
{
	Super::BeginPlay();	
	Snake = Cast<ASnakeActor>(GetWorld()->SpawnActor(SnakeClass));	
}

void ASnakeGameMode::Restart()
{
	Snake->Destroy();
	Snake = Cast<ASnakeActor>(GetWorld()->SpawnActor(SnakeClass));
}
