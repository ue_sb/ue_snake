﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"

#include "SnakeActor.h"
#include "SnakeGameMode.h"
#include "Engine/StaticMesh.h"

void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &ASnakeElementBase::OnOverlap);
}

ASnakeElementBase::ASnakeElementBase()
{	
	PrimaryActorTick.bCanEverTick = false;
}

void ASnakeElementBase::Iteract(ASnakeActor* Snake, ASnakeElementBase* Element)
{	
	// At any point if any of the elements collide with each other, this means certain death for the snake. Note that it might be called twice.
	// Once for the head part and another time for the body part it collided with
	Snake->Kill();
}

void ASnakeElementBase::OnOverlap(AActor* Actor, AActor* OtherActor)
{
	IIteractable* iteractable = Cast<IIteractable>(OtherActor);
	if (iteractable == nullptr) return;
	auto snake = GetWorld()->GetAuthGameMode<ASnakeGameMode>()->Snake;
	iteractable->Iteract(snake, this);
}
