﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Iteractable.h"
#include "GameFramework/Actor.h"
#include "SnakeElementBase.generated.h"

UCLASS()
class SNAKE_API ASnakeElementBase : public AActor, public IIteractable
{
	GENERATED_BODY()

public:
	virtual void BeginPlay() override;	

	ASnakeElementBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector4 TargetPosition;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	FVector4 TargetMovementDirection;

	virtual void Iteract(ASnakeActor* Snake, ASnakeElementBase* Element) override;

	UFUNCTION()
	void OnOverlap(AActor* Actor, AActor* OtherActor);
};
