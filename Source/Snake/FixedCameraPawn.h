#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "FixedCameraPawn.generated.h"

class ASnakeActor;
class UCameraComponent;

UCLASS()
class SNAKE_API AFixedCameraPawn : public APawn
{
	GENERATED_BODY()

public:
	AFixedCameraPawn();

	UPROPERTY(BlueprintReadWrite, DisplayName="Camera", Category="Game")
	UCameraComponent* Camera;
protected:
	virtual void BeginPlay() override;

public:		
	virtual void Tick(float DeltaTime) override;	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	UFUNCTION()
	void OnMoveLeftPressed();	

	UFUNCTION()
	void OnMoveRightPressed();	

	UFUNCTION()
	void OnMoveUpPressed();	

	UFUNCTION()
	void OnMoveDownPressed();
private:
	ASnakeActor* GetSnake();
};
