﻿#include "KillArea.h"

#include "SnakeActor.h"

AKillArea::AKillArea()
{	
	PrimaryActorTick.bCanEverTick = false;
}

void AKillArea::Iteract(ASnakeActor* Snake, ASnakeElementBase* Element)
{
	Snake->Kill();
}


void AKillArea::BeginPlay()
{
	Super::BeginPlay();	
}

