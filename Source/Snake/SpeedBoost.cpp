﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoost.h"

#include "Kismet/GameplayStatics.h"


ASpeedBoost::ASpeedBoost()
{	
	PrimaryActorTick.bCanEverTick = false;
	BoostAmount = 1.5f;
	BoostDuration = 15.0f;
}

void ASpeedBoost::Iteract(ASnakeActor* Snake, ASnakeElementBase* Element)
{
	if (IsUsed) return;
	IsUsed = true;
	UGameplayStatics::SetGlobalTimeDilation(this, BoostAmount);	

	if (ASpeedBoost::timerStopDilation.IsValid()) 
		ASpeedBoost::timerStopDilation.Invalidate();

	GetWorld()->GetTimerManager().SetTimer(ASpeedBoost::timerStopDilation, this, &ASpeedBoost::StopDilation, BoostDuration);

	this->SetActorHiddenInGame(true); // not a proper method to use but whatever
}

void ASpeedBoost::BeginPlay()
{
	Super::BeginPlay();
}

void ASpeedBoost::StopDilation()
{
	if (!timerStopDilation.IsValid()) return;
	UGameplayStatics::SetGlobalTimeDilation(this, 1.0f);
	this->Destroy();
}
