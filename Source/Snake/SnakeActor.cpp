#include "SnakeActor.h"
#include "SnakeElementBase.h"
#include "Iteractable.h"



ASnakeActor::ASnakeActor()
{
	PrimaryActorTick.bCanEverTick = true;
	InitialSize = 4;
	CellSize = 10;
	MovementDirection = FIntVector4(0, 1, 0, 0);
	TargetSize = 4;
	Head = nullptr;
	Tail = nullptr;
	MovementTickInterval = 0.5f;
}

void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetupHeadAndTail();
	BuildSnakeBody();
	GetWorld()->GetTimerManager().SetTimer(timerTick, this, &ASnakeActor::MovementTick, MovementTickInterval, true);
}

void ASnakeActor::Destroyed()
{
	Super::Destroyed();

	for (ASnakeElementBase* element : Elements)
	{
		element->Destroy();
	}	
}

void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// TODO: One could technically add animation here. Won't be easy and will have to be made with dynamic mesh generation or a spline mesh	
}

void ASnakeActor::MovementTick()
{
	if (!timerTick.IsValid()) return;
	MoveSnake(MovementDirection);
	LastMovementDirection = MovementDirection;
	ApplyMovement();
	OnMovementTick.Broadcast();
}

void ASnakeActor::MoveSnake(FIntVector4 Direction)
{
	// This movement method occurs when snake is about to move

	bool needsToMove = true;

	// calculate the deltas to move by
	const auto deltaX = FMath::Clamp(Direction.X, -1, +1) * CellSize;
	const auto deltaY = FMath::Clamp(Direction.Y, -1, +1) * CellSize;

	auto size = GetSnakeSize();

	// Check whether the Snake's size is the same as the Target Snake Size
	// If they are different, we need to either add or remove body parts
	if (TargetSize != size)
	{
		if (TargetSize > size)
		{
			// insertion mode. We just add one body part at a time. We insert it first instead of the head that is moved this tick forward anyway
			auto newElement = Cast<ASnakeElementBase>(GetWorld()->SpawnActor(BodyClass));

			newElement->TargetPosition = Elements[0]->TargetPosition;
			Elements[0]->TargetPosition = Elements[0]->TargetPosition + FVector4(deltaX, deltaY, 0, 0);

			Elements[0]->SetActorLocation(Elements[0]->TargetPosition);
			newElement->SetActorLocation(newElement->TargetPosition);

			Elements.Insert(newElement, 1);
			// SNAKE DOES NOT needs to move in this case.
			// Only two elements of the snake will change their position: the new one will have it set and the head will move one cell forward
			needsToMove = false;
		}

		if (TargetSize < size)
		{
			// Removal mode. We remove any amount of body parts starting from its pre-tail
			if (TargetSize < 2)
			{
				// If don't have head and tail, we're dead.
				Kill();
				return;
			}

			for (int i = 0; i < size - TargetSize; i++)
			{
				auto el = Elements[Elements.Num() - 1];
				Elements.Remove(el);
				GetWorld()->DestroyActor(el);
			}
		}


	}

	// Movement bit

	if (needsToMove)
	{
		TArray<FVector4> newLocations;
		bool isFirst = true;
		FVector4 previousElementLocation;

		// first loop to only *calculate* future positions
		for (ASnakeElementBase* element : Elements)
		{
			if (isFirst)
			{
				// Z is upwards so we're good with X & Y only
				newLocations.Add(element->TargetPosition + FVector4(deltaX, deltaY, 0, 0));
				previousElementLocation = element->TargetPosition;
				isFirst = false;
			}
			else
			{
				// Repeat the position of each previous element
				newLocations.Add(previousElementLocation);
				previousElementLocation = element->TargetPosition;
			}
		}

		// we have all the locations calculated so let's set them anew and calculate directions to move (for possible animation)
		for (int i = 0; i < Elements.Num(); i++)
		{
			auto PreviousPosition = Elements[i]->TargetPosition;
			Elements[i]->TargetPosition = newLocations[i];

			if (i != 0)
				Elements[i]->TargetMovementDirection = (Elements[i]->TargetPosition - PreviousPosition).GetSafeNormal();
			else // Custom 'Direction' for the head
				Elements[i]->TargetMovementDirection = FVector4(Direction.X, Direction.Y, 0, 0);
		}
	}

}

void ASnakeActor::ApplyMovement()
{
	for (ASnakeElementBase* element : Elements)
	{
		FVector4 newElementPosition = element->TargetPosition;
		element->SetActorLocation(newElementPosition);
	}
}

void ASnakeActor::Kill()
{
	// when we're dead we no longer need to move (spawn a new SnakeActor if you intend to restart the game)	
	timerTick.Invalidate();
	OnDeath.Broadcast();
}


FIntVector4 ASnakeActor::GetLastAppliedMovementDirection()
{
	return LastMovementDirection;
}

int ASnakeActor::GetSnakeSize()
{
	return Elements.Num();
}

bool ASnakeActor::IsAlive()
{
	return timerTick.IsValid();
}

void ASnakeActor::SetupHeadAndTail()
{
	if (Elements.Num() > 0) return; // don't want to do it again, do we?
	Head = Cast<ASnakeElementBase>(GetWorld()->SpawnActor(HeadClass));
	Tail = Cast<ASnakeElementBase>(GetWorld()->SpawnActor(TailClass));
	Elements.Add(Head);
	Elements.Add(Tail);

	auto direction = this->GetActorForwardVector() * -1; // let the direction we spawn stuff be 'behind' our current forward direction
	Head->SetActorLocation(this->GetActorLocation());
	Head->SetActorRotation(this->GetActorRotation());

	Tail->SetActorLocation(this->GetActorLocation() + (InitialSize - 1) * CellSize * direction); // tail is already positioned correctly at the very end of the centipede
	Tail->SetActorRotation(this->GetActorRotation());
}

void ASnakeActor::BuildSnakeBody()
{
	auto direction = this->GetActorForwardVector() * -1;
	for (int i = 2; i < InitialSize; i++)
	{
		auto element = Cast<ASnakeElementBase>(GetWorld()->SpawnActor(BodyClass));
		element->TargetPosition = direction * CellSize * (i - 1);
		element->SetActorLocation(element->TargetPosition);
		Elements.Insert(element, Elements.Num() - 1);
	}
}