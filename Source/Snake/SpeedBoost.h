﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Iteractable.h"
#include "GameFramework/Actor.h"
#include "SpeedBoost.generated.h"

UCLASS()
class SNAKE_API ASpeedBoost : public AActor, public IIteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpeedBoost();
	
	bool IsUsed = false;
	
	FTimerHandle timerStopDilation;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BoostAmount;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float BoostDuration;	

	UFUNCTION()
	void StopDilation();

	virtual void Iteract(ASnakeActor* Snake, ASnakeElementBase* Element) override;
protected:
	virtual void BeginPlay() override;
};
