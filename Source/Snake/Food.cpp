﻿#include "Food.h"

#include "SnakeActor.h"

AFood::AFood()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AFood::BeginPlay()
{
	Super::BeginPlay();	
}

void AFood::Iteract(ASnakeActor* Snake, ASnakeElementBase* Element)
{
	Snake->TargetSize++;
	this->Destroy();
}

