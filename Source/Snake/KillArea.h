﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Iteractable.h"
#include "GameFramework/Actor.h"
#include "KillArea.generated.h"

UCLASS()
class SNAKE_API AKillArea : public AActor, public IIteractable
{
	GENERATED_BODY()

public:	
	AKillArea();

	virtual void Iteract(ASnakeActor* Snake, ASnakeElementBase* Element) override;
protected:	
	virtual void BeginPlay() override;	
};
