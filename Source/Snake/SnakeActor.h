#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineMeshComponent.h"
#include "SnakeActor.generated.h"

class ASnakeElementBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeathDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMovementDelegate);

UCLASS()
class SNAKE_API ASnakeActor : public AActor
{
	GENERATED_BODY()

public: 
	// EVENTS

	// Event that is triggered when the Snake dies
	UPROPERTY(BlueprintAssignable, Category = Events)
	FDeathDelegate OnDeath;

	// Event that is triggered at each Snake movement tick
	UPROPERTY(BlueprintAssignable, Category = Events)
	FMovementDelegate OnMovementTick;
protected:
	virtual void BeginPlay() override;	
	virtual void Destroyed() override;
public:	
	ASnakeActor();

	virtual void Tick(float DeltaTime) override;	

	// Array of references to ALL the elements in the snake (includes body and tail)
	UPROPERTY()
	TArray<ASnakeElementBase*> Elements;

	// Class of the Head element of the snake to instantiate
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Elements)
	TSubclassOf<ASnakeElementBase> HeadClass;

	// Class of the Body element of the snake to instantiate
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Elements)
	TSubclassOf<ASnakeElementBase> BodyClass;
								  
	// Class of the Tail element of the snake to instantiate
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Elements)
	TSubclassOf<ASnakeElementBase> TailClass;

	// Reference to the head element of the snake
	UPROPERTY()
	ASnakeElementBase* Head;

	// Reference to the tail element of the snake
	UPROPERTY()
	ASnakeElementBase* Tail;
	
	// Size of the cell the snake travels with each movement tick
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	float CellSize;	

	// Initial size of the snake
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	int32 InitialSize;

	// How often does the physical movement is triggered. Affected by time dilation.
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MovementTickInterval;	
	
	// Current Movement direction. Should be either
	// (0, �1, 0, 0)
	// or 
	// (�1, 0, 0, 0)
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FIntVector4 MovementDirection;
		
	// The current 'target size' of the snake. While we can remove *all* the body parts almost instantaneously, adding them at the front would not be feasible
	// from the control point of view, adding them at the back would cause collision issues (when the tail will suddently grow and hit the body or any other part of the
	// playfield anyway)	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 TargetSize;

	// Kills the snake
	UFUNCTION()
	void Kill();

	// Returns current snake size. Useful for UI or game state tracking for example			
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int GetSnakeSize();	
	
	// Returns the last movement direction that was actually applied to the Snake
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FIntVector4 GetLastAppliedMovementDirection();

	// Returns whether the Snake is alive
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsAlive(); 	

private:
	// Initialises Head and Tail of the Snake
	void SetupHeadAndTail();
	
	// Builds the Snake's body based on its initial size (does not create Head or Tail)
	UFUNCTION()
	void BuildSnakeBody();

	// Moves the snake using the Direction specified
	UFUNCTION()
	void MoveSnake(FIntVector4 Direction);

	// Last direction the snake actually moved
	FIntVector4 LastMovementDirection;

	// Timer to handle movement tick
	FTimerHandle timerTick;

	// The method called by the timerTick
	UFUNCTION()
	void MovementTick();	

	// Applies movement that was calculated during MovementTick
	UFUNCTION()
	void ApplyMovement();
};
