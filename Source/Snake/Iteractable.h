// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Iteractable.generated.h"

class ASnakeActor;
class ASnakeElementBase;

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UIteractable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SNAKE_API IIteractable
{
	GENERATED_BODY()

public:	
	UFUNCTION()
	virtual void Iteract(ASnakeActor* Snake, ASnakeElementBase* Element);
};
