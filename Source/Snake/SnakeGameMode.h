#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "SnakeGameMode.generated.h"

class ASnakeActor;
UCLASS()
class SNAKE_API ASnakeGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ASnakeGameMode();

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Game Mode")
	ASnakeActor* Snake;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Game Mode")
	TSubclassOf<ASnakeActor> SnakeClass;

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void Restart();
};
