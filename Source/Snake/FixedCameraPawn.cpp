#include "FixedCameraPawn.h"

#include "SnakeActor.h"
#include "SnakeGameMode.h"
#include "Camera/CameraComponent.h"

AFixedCameraPawn::AFixedCameraPawn()
{ 	
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));	
}

void AFixedCameraPawn::BeginPlay()
{
	Super::BeginPlay();
	FRotator FacingDown(-90.0, -90.0, 0.0);
	Camera->SetWorldRotation(FacingDown);
}

void AFixedCameraPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFixedCameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction(TEXT("MoveLeft"), IE_Pressed, this, &AFixedCameraPawn::OnMoveLeftPressed);
	PlayerInputComponent->BindAction(TEXT("MoveRight"), IE_Pressed, this, &AFixedCameraPawn::OnMoveRightPressed);
	PlayerInputComponent->BindAction(TEXT("MoveUp"), IE_Pressed, this, &AFixedCameraPawn::OnMoveUpPressed);
	PlayerInputComponent->BindAction(TEXT("MoveDown"), IE_Pressed, this, &AFixedCameraPawn::OnMoveDownPressed);
}

void AFixedCameraPawn::OnMoveLeftPressed()
{
	auto snake = GetSnake();
	if (snake->GetLastAppliedMovementDirection().X == 1) return;
	snake->MovementDirection = UE::Math::TIntVector4(-1, 0, 0, 0);
}

void AFixedCameraPawn::OnMoveRightPressed()
{
	auto snake = GetSnake();
	if (snake->GetLastAppliedMovementDirection().X == -1) return;
	snake->MovementDirection = UE::Math::TIntVector4(1, 0, 0, 0);
}

void AFixedCameraPawn::OnMoveUpPressed()
{
	auto snake = GetSnake();
	if (snake->GetLastAppliedMovementDirection().Y == 1) return;
	snake->MovementDirection = UE::Math::TIntVector4(0, -1, 0, 0);
}

void AFixedCameraPawn::OnMoveDownPressed()
{
	auto snake = GetSnake();
	if (snake->GetLastAppliedMovementDirection().Y == -1) return;
	snake->MovementDirection = UE::Math::TIntVector4(0, 1, 0, 0);
}

ASnakeActor* AFixedCameraPawn::GetSnake()
{
	return GetWorld()->GetAuthGameMode<ASnakeGameMode>()->Snake;
}

